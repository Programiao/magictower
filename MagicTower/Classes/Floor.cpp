#include "Floor.h"
#include "actions\CCActionInstant.h"

Floor::Floor(void)
{
}

Floor::~Floor(void)
{
	m_gameScene->release();
	m_map->release();
}

bool Floor::init( int left, int bottom, int index, CCTMXTiledMap* parentMap )
{
	m_left = left;
	m_bottom = bottom;
	m_layer = NULL;
	parentMap->retain();
	m_map = parentMap;
	m_gameScene = (CCLayer*)parentMap->getParent();
	m_gameScene->retain();

	//init m_layer
	char str[100] = {0};
	sprintf(str, "F%d", index+1);
	CCLog(str);
	m_layer = m_map->layerNamed(str);
	ERROR_LOG_RETURN(m_layer, "floor : init floor failed");
	m_layer->setVisible(false);
	//init m_animLayer
	sprintf(str, "A%d", index+1);
	m_animLayer = m_map->layerNamed(str);
	ERROR_LOG_RETURN(m_layer, "floor : init animLayer failed");
	m_animLayer->setVisible(false);

	//init tiles
	CCSize mSize = m_map->getMapSize();
	m_size = mSize;

	for (int col = 0; col < mSize.width; col++)
	{
		for (int row = 0; row < mSize.height; row++)
		{
			int gid = m_layer->tileGIDAt(ccp(col, row));
			CCLog("(%d, %d), %d", col, row, gid);
			if (gid)
			{
				processTileInfo(gid, ccp(col, row));
			}

		}
	}
	return true;
}

CCTMXLayer* Floor::getLayer()
{
	return m_layer;
}

void Floor::setVisible( bool v )
{
	m_layer->setVisible(v);
	m_animLayer->setVisible(v);
}

bool Floor::processTileInfo(int gid, CCPoint point)
{
	CCDictionary* dic = m_map->propertiesForGID(gid);
	bool ifAnim = dic->valueForKey("anim")->boolValue();

	if (ifAnim)		//has animation
	{
		bool animRow = animRow = dic->valueForKey("animRow")->boolValue();
		if (animRow)
		{
			//because animation has some problem within the same layer
			//I move animation to another layer
			//maybe tiles in CCTMXLayer shouldn't be used for animation
			m_layer->removeTileAt(point);
			m_animLayer->setTileGID(gid, point);
			CCSprite* spr = m_animLayer->tileAt(point);
			animateTile(gid, spr);
		}
	}
	return true;
}
		

CCDictionary* Floor::getInfoAt( CCPoint pos )
{
	pos.y = m_size.height - pos.y - 1;
	int gid = m_layer->tileGIDAt(pos);
	if (gid == 0)
	{
		gid = m_animLayer->tileGIDAt(pos);
		if(gid == 0) 
			return NULL;
	}
	return m_map->propertiesForGID(gid);
}

void Floor::removeObjAt( CCPoint pos )
{
	pos.y = m_size.height - pos.y - 1;
	m_layer->removeTileAt(pos);
}

bool Floor::animateTile( int gid, CCSprite* sprite )
{
	//init texture
	CCTexture2D* tex = CCTextureCache::sharedTextureCache()->addImage("monsters.png");

	//because there is only one tile set image, get the positions by gid
	gid -= 1;		//make gid begin with 0
	int beginRow = gid / TILE_SET_WIDTH;
	int beginCol = gid - beginRow * TILE_SET_WIDTH;
	int beginX = beginCol * TILE_WIDTH;
	int beginY = beginRow * TILE_WIDTH;

	//init frames array
	CCArray* sprFrames = CCArray::createWithCapacity(4);
	ERROR_LOG_RETURN(sprFrames, "animated sprite : create array failed");

	for (int index = 0; index < 4; index++)
	{
		CCSpriteFrame* frame = NULL;
		int x = beginX + index * TILE_WIDTH;
		frame = CCSpriteFrame::createWithTexture(tex, 
			CCRect(x, beginY, TILE_WIDTH, TILE_WIDTH));
		sprFrames->addObject(frame);
	}

	//init animation
	CCAnimation* playerAnim =  CCAnimation::createWithSpriteFrames(sprFrames, 0.2f);
	ERROR_LOG_RETURN(playerAnim, "animated sprite : create animation failed");

	//init action
	CCRepeatForever* act  = CCRepeatForever::create(CCAnimate::create(playerAnim));
	ERROR_LOG_RETURN(act, "animated sprite : create action failed");

	sprite->runAction(act);
	return true;
}

bool Floor::playAnimation( CCPoint pos )
{
	pos.y = m_size.height - pos.y - 1;
	int gid = m_layer->tileGIDAt(pos);
	CCSprite* sprite = m_layer->tileAt(pos);

	//init texture
	CCTexture2D* tex = CCTextureCache::sharedTextureCache()->addImage("monsters.png");

	//because there is only one tile set image, get the positions by gid
	gid -= 1;		//make gid begin with 0
	int beginRow = gid / TILE_SET_WIDTH;
	int beginCol = gid - beginRow * TILE_SET_WIDTH;
	int beginX = beginCol * TILE_WIDTH;
	int beginY = beginRow * TILE_WIDTH;

	//init frames array
	CCArray* sprFrames = NULL;
	sprFrames = CCArray::createWithCapacity(4);
	ERROR_LOG_RETURN(sprFrames, "animated sprite : create array failed");

	for (int index = 0; index < 4; index++)
	{
		CCSpriteFrame* frame = NULL;
		int y = beginY + index * TILE_WIDTH;
		frame = CCSpriteFrame::createWithTexture(tex, 
			CCRect(beginX, y, TILE_WIDTH, TILE_WIDTH));
		sprFrames->addObject(frame);
	}

	//init animation
	CCAnimation* playerAnim = NULL;
	playerAnim = CCAnimation::createWithSpriteFrames(sprFrames, 0.06f);
	ERROR_LOG_RETURN(playerAnim, "add wait anim : create animation failed");

	CCAnimate* animate = NULL;
	animate = CCAnimate::create(playerAnim);
	ERROR_LOG_RETURN(animate, "add wait anim : create animate failed");

	CCSequence* seq = CCSequence::create(animate, CCCallFunc::create(this, callfunc_selector(Floor::removeCallback)), NULL);
	sprite->runAction(seq);
	
	m_pToRemove = pos;
	return true;
}

void Floor::removeCallback()
{
	m_layer->removeTileAt(m_pToRemove);
}
