#ifndef __GAMESCENE_H__
#define __GAMESCENE_H__

#include "Global.h"
#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "Player.h"
#include "Floor.h"

class GameScene : public cocos2d::CCLayer
{
private:
	Player* m_player;
	int m_floorNum;
	int m_currFloor;
	int m_mapLeft;
	int m_mapBottom;
	int m_colNum;
	int m_rowNum;
	CCTMXTiledMap* m_bgMap;
	CCTMXTiledMap* m_floorsMap;
	Floor* m_floors;

	CCLabelTTF* m_infoLabel;
	CCSprite*  m_infoLayer;

	bool m_canMove;

	bool beginFight();
	bool inScope(CCPoint point);
	void processTileInfo(CCDictionary* info, CCPoint targetPos, int dir);
	void showInfo(std::wstring info);
public:
	static cocos2d::CCScene* scene();

	virtual bool init();  
	bool initBackground();
	bool initPlayer();
	bool initFloors();

	void onUpdate(float dt);

	CREATE_FUNC(GameScene);
};

#endif //__GAMESCENE_H__