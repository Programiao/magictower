#include "MainMenuScene.h"
#include "GameScene.h"

using namespace cocos2d;

CCScene* MainMenu::scene()
{
    CCScene * scene = NULL;
    do 
    {
        // 'scene' is an autorelease object
        scene = CCScene::create();
        CC_BREAK_IF(! scene);

        // 'layer' is an autorelease object
        MainMenu *layer = MainMenu::create();
        CC_BREAK_IF(! layer);

        // add layer as a child to scene
        scene->addChild(layer);
    } while (0);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool MainMenu::init()
{
    bool bRet = false;
    do 
    {
	//init super first
        CC_BREAK_IF(! CCLayer::init());


	//create menu items
	CCMenuItemImage* start = CCMenuItemImage::create(
					"start_checked.png", 
					"start.png", 
					this, 
					menu_selector(MainMenu::menuStartCallback));
	start->setPosition(352, 250);
	CCMenuItemImage* quit = CCMenuItemImage::create(
					"quit_checked.png", 
					"quit.png", 
					this, 
					menu_selector(MainMenu::menuCloseCallback));
	quit->setPosition(352, 180);

	CCMenu* menu = CCMenu::create(start, quit, NULL);
	menu->setPosition(CCPointZero);
	this->addChild(menu);


        bRet = true;
    } while (0);

    return bRet;
}

void MainMenu::menuCloseCallback(CCObject* pSender)
{
    // "close" menu item clicked
    CCDirector::sharedDirector()->end();
}

void MainMenu::menuStartCallback( CCObject* pSender )
{
	// "start" menu item clicked
	CCDirector* dir = CCDirector::sharedDirector();
	//dir->replaceScene(GameScene::scene());
	dir->pushScene(GameScene::scene());
}


