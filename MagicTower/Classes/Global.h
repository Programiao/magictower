#ifndef __GLOBAL_H__
#define  __GLOBAL_H__


#include "cocos2d.h"
#include "SimpleAudioEngine.h"
using namespace cocos2d;

#define ERROR_RETURN_FALSE(x) if(!(x)) return false;
#define ERROR_LOG_RETURN(x, y) if(!(x)) {CCLog(y);return false;}

#define DOWN 0
#define LEFT 1
#define RIGHT 2
#define UP 3

#define FOR(i,n) for(int i = 0; i < n; i++)

//tile type
#define WALL 0
#define MONSTER 1

#define KEY_YELLOW 3
#define KEY_BLUE 4
#define KEY_RED 5

#define DOOR_YELLOW 6
#define DOOR_BLUE 7
#define DOOR_RED 8

#define FLOOR_UP 9
#define FLOOR_DOWN 10

#define TILE_SET_WIDTH 32
#define TILE_WIDTH 32

#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include <windows.h>
#define KEY_DOWN(vk_code) (GetAsyncKeyState(vk_code) & 0x8000 ? 1 : 0)
#define KEY_UP(vk_code) (GetAsyncKeyState(vk_code) & 0x8000 ? 0 : 1)
#endif

#define AddPoints(a, b) ccp( a.x + b.x, a.y + b.y);

std::string WStrToUTF8(const std::wstring& str);

#endif //__GLOBAL_H__