#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "Global.h"
#include "cocos2d.h"
#include "SimpleAudioEngine.h"

using namespace cocos2d;

class Player
{
public:
	enum{YELLOW, BLUE, RED};
	CCPoint m_gridPos;
	int m_hp;
	CCLabelTTF* m_hpLabel;
	CCLabelTTF* m_hpValueLabel;
	int m_att;
	CCLabelTTF* m_attLabel;
	CCLabelTTF* m_attValueLabel;
	int m_def;
	CCLabelTTF* m_defLabel;
	CCLabelTTF* m_defValueLabel;
	int m_gold;
	CCLabelTTF* m_goldLabel;
	CCLabelTTF* m_goldValueLabel;
	int m_exp;
	CCLabelTTF* m_expLabel;
	CCLabelTTF* m_expValueLabel;
	int m_level;
	CCLabelTTF* m_levelLabel;
	CCLabelTTF* m_levelValueLabel;

	int m_keys[3];
	CCLabelTTF* m_keyValueLabels[3];
	/*CCLabelTTF* m_ykeyValueLabel;
	CCLabelTTF* m_bkeyValueLabel;
	CCLabelTTF* m_rkeyValueLabel;*/
	
	//padding
	int m_left;
	int m_bottom;
	CCSprite* m_sprite;
	CCLayer* m_gameScene;

	bool initSprite();
	CCPoint toPixelPos(CCPoint point);
	CCAnimate* createAction(int dir);
	bool initInfoPanel();
	CCLabelTTF* createALabel( int x, int y, std::string& text );
	void refreshKeyValue();

public:
	Player(CCLayer* gameScene, CCDictionary* info);
	Player(const char* saveFile);
	~Player();

	
	CCSprite* getSprite();
	void setGridPosition(int col, int row);
	CCPoint getGridPosition();
	void move(int dir);

	bool canAttack(CCDictionary* info);
	void addItem(int tp);
	
	//@param type   KEY_YELLOW, KEY_BLUE or KEY_RED
	void addKey(int type);
	//@param type   KEY_YELLOW, KEY_BLUE or KEY_RED
	bool removeKey(int type);
};

#endif //__PLAYER_H__