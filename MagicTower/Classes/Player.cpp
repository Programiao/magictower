#include "Player.h"


Player::Player(CCLayer* gameScene, CCDictionary* info)
{
	m_hp = info->valueForKey("hp")->intValue();
	m_att = info->valueForKey("att")->intValue();
	m_def = info->valueForKey("def")->intValue();
	m_exp = 0;
	m_gold = 0;
	m_level = 1;

	FOR(i, 3)
		m_keys[i] = 1;

	m_gameScene = gameScene;
	m_gameScene->retain();

	initSprite();
	//set padding
	m_left = info->valueForKey("mapLeft")->intValue();
	m_bottom = info->valueForKey("mapBottom")->intValue();
	//set position
	setGridPosition(info->valueForKey("playerX")->intValue(), info->valueForKey("playerY")->intValue());
	m_sprite->setAnchorPoint(CCPointZero);
	//add to scene
	m_gameScene->addChild(m_sprite, 150);


	initInfoPanel();

}

Player::Player( const char* saveFile )
{

}

Player::~Player()
{
	//to do : some clear
	m_gameScene->release();
}

bool Player::initSprite()
{
	//init texture
	CCTexture2D* texPlayer = CCTextureCache::sharedTextureCache()->addImage("Braver.png");
	ERROR_RETURN_FALSE(texPlayer);

	//get image size, calculate one frame size
	CCSize texSize = texPlayer->getContentSize();
	float framew = texSize.width / 4.0f;
	float frameh = texSize.height / 4.0f;

	m_sprite = CCSprite::createWithTexture(texPlayer, CCRect(0, 0, framew, frameh));
	ERROR_RETURN_FALSE(m_sprite);

	CCSprite* panelPlayer = CCSprite::createWithTexture(texPlayer, CCRect(0, 0, framew, frameh));
	panelPlayer->setPosition(ccp(30, 400));
	panelPlayer->setAnchorPoint(CCPointZero);
	panelPlayer->setScale(1.5);
	m_gameScene->addChild(panelPlayer, 500);

	return true;
}

CCSprite* Player::getSprite()
{
	return m_sprite;
}

void Player::setGridPosition( int col, int row )
{
	m_gridPos = ccp(col, row);
	m_sprite->setPosition(toPixelPos(m_gridPos));
}

cocos2d::CCPoint Player::toPixelPos( CCPoint point )
{
	return ccp(point.x * TILE_WIDTH + m_left, point.y * TILE_WIDTH + m_bottom);
}

void Player::move( int dir )
{
	CCLog("player : move %d", dir);
	CCPoint delta[4] = {ccp(0, -1), ccp(-1, 0), ccp(1, 0), ccp(0, 1)};
	CCPoint after = ccp(m_gridPos.x + delta[dir].x, m_gridPos.y + delta[dir].y);
	m_sprite->runAction(createAction(dir));

	m_gridPos = after;
	CCActionInterval* moveAction = CCMoveTo::create(0.07f, toPixelPos(after));
	m_sprite->runAction(moveAction);
}

CCAnimate* Player::createAction( int dir )
{
	CCTexture2D* texPlayer = CCTextureCache::sharedTextureCache()->addImage("Braver.png");

	//get image size, calculate one frame size
	CCSize texSize = texPlayer->getContentSize();
	float framew = texSize.width / 4.0f;
	float frameh = texSize.height / 4.0f;

	CCArray* arrayFrames = CCArray::createWithCapacity(4);
	for (int j = 0; j < 4; j++)
	{
		CCSpriteFrame* sp = CCSpriteFrame::createWithTexture(texPlayer, 
			CCRect(j * framew, dir * frameh, framew, frameh));
		arrayFrames->addObject(sp);
	}
	CCSpriteFrame* sp = CCSpriteFrame::createWithTexture(texPlayer, 
		CCRect(0, dir * frameh, framew, frameh));
	arrayFrames->addObject(sp);

	CCAnimation* playerAnim = NULL;
	playerAnim = CCAnimation::createWithSpriteFrames(arrayFrames, 0.04f);
	CCAnimate* animate = NULL;
	animate = CCAnimate::create(playerAnim);
	return animate;
}

cocos2d::CCPoint Player::getGridPosition()
{
	return m_gridPos;
}

bool Player::canAttack( CCDictionary* info )
{
	int hp = info->valueForKey("hp")->intValue();
	int att = info->valueForKey("att")->intValue();
	int def = info->valueForKey("def")->intValue();
	//��Ҳ����Ʒ�
	if (m_att <= def)
		return false;
	//��ҷ������ڹ��﹥����
	if (m_def >= att)
		return true;
	//�໥����
	int times = hp / (m_att - def);
	int lost = times * (att - m_def);
	CCLog("lost:%d", lost);
	if (lost < m_hp)
		return true;
	else
		return false;
}

void Player::addItem( int tp )
{
	switch(tp)
	{
	default:
		break;
	}
}


bool Player::initInfoPanel()
{
	int x1 = 30;
	int x2 = 110;
	int y = 365;
	int dy = 25;

	char str[100] = {0};
	sprintf(str, "%d", m_level);
	m_levelValueLabel = createALabel(100, 400, std::string(str));
	m_levelLabel = createALabel(140, 400, WStrToUTF8(L"��"));

	m_hpLabel = createALabel(x1, y, WStrToUTF8(L"���� :"));
	sprintf(str, "%d", m_hp);
	m_hpValueLabel = createALabel(x2, y, std::string(str));
	
	y -= dy;
	m_attLabel = createALabel(x1, y, WStrToUTF8(L"���� :"));
	sprintf(str, "%d", m_att);
	m_attValueLabel = createALabel(x2, y, std::string(str) );

	y -= dy;
	m_defLabel = createALabel(x1, y, WStrToUTF8(L"���� :"));
	sprintf(str, "%d", m_def);
	m_defValueLabel = createALabel(x2, y, std::string(str));

	y -= dy;
	m_expLabel = createALabel(x1, y, WStrToUTF8(L"���� :"));
	sprintf(str, "%d", m_exp);
	m_expValueLabel = createALabel(x2, y, std::string(str));

	y -= dy;
	m_goldLabel = createALabel(x1, y, WStrToUTF8(L"��� :"));
	sprintf(str, "%d", m_gold);
	m_goldValueLabel = createALabel(x2, y, std::string(str));

	x2 = 80;
	FOR(i, 3)
	{
		y -= 32;
		sprintf(str, "%d", m_keys[i]);
		m_keyValueLabels[i] = createALabel(x2, y, std::string(str));
	}
	return true;
}

CCLabelTTF* Player::createALabel( int x, int y, std::string& text )
{
	CCLabelTTF* label = CCLabelTTF::create(text.c_str(), "�����ͨ", 22);
	label->setPosition(ccp(x, y));
	label->setAnchorPoint(CCPointZero);
	m_gameScene->addChild(label, 500);
	return label;
}

void Player::addKey( int type )
{
	type -= 3;
	m_keys[type]++;
	CCLog("get key %d", type);
	refreshKeyValue();
}

bool Player::removeKey( int type )
{
	type -= 3;
	if(m_keys[type] == 0)
		return false;
	m_keys[type]--;
	refreshKeyValue();
	CCLog("use key %d", type);
	return true;
}

void Player::refreshKeyValue()
{
	FOR(i, 3)
	{
		char str[100] = {0};
		sprintf(str, "%d", m_keys[i]);
		m_keyValueLabels[i]->setString(str);
	}
}
