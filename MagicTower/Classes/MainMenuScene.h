#ifndef __MAINMENU_H__
#define __MAINMENU_H__

#include "cocos2d.h"

#include "SimpleAudioEngine.h"

class MainMenu : public cocos2d::CCLayer
{
public:
    virtual bool init();  
    static cocos2d::CCScene* scene();
    
    // menu selector callback
    void menuCloseCallback(CCObject* pSender);
    void menuStartCallback(CCObject* pSender);

    // implement the "static node()" method manually
    CREATE_FUNC(MainMenu);
};

#endif  // __MAINMENU_H__