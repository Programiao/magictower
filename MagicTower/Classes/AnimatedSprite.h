#ifndef __ANIMATEDSPRITE_H__
#define __ANIMATEDSPRITE_H__

#include "Global.h"

class AnimatedSprite
{
private:
	CCSprite* m_sprite;
	CCRepeatForever* m_action;

public:
	AnimatedSprite(void);
	~AnimatedSprite(void);

	bool init(int gid, int x, int y, bool dirRow, CCSprite* sprite);
	CCSprite* getSprite();
};

#endif //__ANIMATEDSPRITE_H__
