#include "GameScene.h"

/*
to do: memory leak somewhere 
*/
/************************************************************************
 info layer z = 1000                                                                                               
 info label z = 1100                                                                                               
 map z = 100   
 label left z = 500
************************************************************************/

cocos2d::CCScene* GameScene::scene()
{
	CCScene * scene = NULL;
	do 
	{
		scene = CCScene::create();
		CC_BREAK_IF(! scene);

		GameScene *layer = GameScene::create();
		CC_BREAK_IF(! layer);

		scene->addChild(layer);
	} while (0);

	// return the scene
	return scene;
}

bool GameScene::init()
{
	bool bRet = false;
	do 
	{
		//init super first
		CC_BREAK_IF(! CCLayer::init());
		//add background
		CC_BREAK_IF(! initBackground());
		//init player
		CC_BREAK_IF(! initPlayer());
		//init floors
		CC_BREAK_IF(! initFloors());

		schedule(schedule_selector(GameScene::onUpdate));
		
		m_canMove = true;

		bRet = true;
	} while (0);

	return bRet;
}

bool GameScene::initBackground()
{
	m_bgMap = NULL;
	m_bgMap = CCTMXTiledMap::create("bg.tmx");
	ERROR_LOG_RETURN(m_bgMap, "game scene : init background failed");
	

	this->addChild(m_bgMap, 1);
	return true;
}

bool GameScene::initPlayer()
{
	// get information from map
	CCDictionary* dic = m_bgMap->getProperties();
	

	m_player = NULL;
	m_player = new Player(this, dic);

	return true;
}

bool GameScene::initFloors()
{
	//get floor info
	CCDictionary* dic = m_bgMap->getProperties();
	m_floorNum = dic->valueForKey("floorNum")->intValue();
	m_mapLeft = dic->valueForKey("mapLeft")->intValue();
	m_mapBottom = dic->valueForKey("mapBottom")->intValue();

	//init  info label
	m_infoLabel = CCLabelTTF::create();
	m_infoLabel->setFontName("迷你简卡通");
	m_infoLabel->setFontSize(30);
	int labelx = dic->valueForKey("infoLabelX")->intValue();
	labelx = labelx * TILE_WIDTH;
	int labely = dic->valueForKey("infoLabelY")->intValue();
	labely = labely * TILE_WIDTH;
	m_infoLabel->setPosition(ccp(labelx, labely));
	this->addChild(m_infoLabel, 1100);
	//m_infoLabel->setVisible(false);

	m_infoLayer = CCSprite::create("info.png");
	m_infoLayer->setVisible(false);
	m_infoLayer->setAnchorPoint(ccp(0, 0.5));
	m_infoLayer->setPosition(ccp(m_mapLeft+TILE_WIDTH*2 , labely));
	this->addChild(m_infoLayer, 1000);


	m_floors = NULL;
	m_floors = new Floor[m_floorNum];
	ERROR_LOG_RETURN(m_floors, "game scene : alloc floors failed");

	//load floors
	m_floorsMap = NULL;
	m_floorsMap = CCTMXTiledMap::create("floor.tmx");
	ERROR_LOG_RETURN(m_floorsMap, "game-scene : create floors failed");
	this->addChild(m_floorsMap, 100);


	CCSize size = m_floorsMap->getMapSize();
	m_rowNum = size.height;
	m_colNum = size.width;

	m_floorsMap->setAnchorPoint(CCPointZero);
	m_floorsMap->setPosition(ccp(m_mapLeft, m_mapBottom));

	//init every floor
	for (int i = 0; i < m_floorNum; i++)
	{
		bool t = m_floors[i].init(m_mapLeft, m_mapBottom, i, m_floorsMap);
		ERROR_RETURN_FALSE(t);
	}

	m_floors[0].setVisible(true);
	m_currFloor = 0;

	return true;
}

void GameScene::onUpdate( float dt )
{
	static int process = 0; 
	//check every 7 frame
	if (process <= 0)
	{
		process = 6;
		int key[4] = {VK_DOWN, VK_LEFT, VK_RIGHT, VK_UP};
		CCPoint delta[4] = {ccp(0, -1), ccp(-1, 0), ccp(1, 0), ccp(0, 1)};
		for (int i = 0; i < 4; i++)
		{
			if (KEY_DOWN(key[i]))
			{
				CCPoint playerPos = m_player->getGridPosition();
				CCPoint targetPos = AddPoints(playerPos, delta[i]);
				if (inScope(targetPos))
				{
					CCDictionary* info = m_floors[m_currFloor].getInfoAt(targetPos);
					//empty tile
					if(info == NULL)
					{
						m_player->move(i);
					}
					else //has something
					{
						processTileInfo(info, targetPos, i);
					}

				}
				
				break;
			}
		}
	}
	process--;
	// in case of overflow
	if (process > 10)
	{
		process = 0;
	}
}

bool GameScene::inScope( CCPoint point )
{
	return (point.x > -1) && (point.x < m_colNum) && (point.y > -1) && (point.y < m_rowNum);
}

void GameScene::processTileInfo( CCDictionary* info, CCPoint targetPos, int dir )
{
	int tp = info->valueForKey("type")->intValue();
	switch(tp)
	{
	case WALL:
		break;
	case MONSTER:
		if (m_player->canAttack(info))
		{
			CCLog("can attack!");
		}
		break;
	case KEY_YELLOW:
		{
			m_player->addKey(KEY_YELLOW);
			m_floors[m_currFloor].removeObjAt(targetPos);
			m_player->move(dir);
			showInfo(L"得到一把黄钥匙");
		}

		break;
	case KEY_BLUE:
		{
			m_player->addKey(KEY_BLUE);
			m_floors[m_currFloor].removeObjAt(targetPos);
			m_player->move(dir);
			showInfo(L"得到一把蓝钥匙");
		}
		break;
	case KEY_RED:
		{
			m_player->addKey(KEY_RED);
			m_floors[m_currFloor].removeObjAt(targetPos);
			m_player->move(dir);
			showInfo(L"得到一把红钥匙");
		}
		
		break;
	case DOOR_YELLOW:
	case DOOR_BLUE:
	case DOOR_RED:
		if(m_player->removeKey(tp - 3))
		{
			m_floors[m_currFloor].playAnimation(targetPos);
		}
		break;
	default:
		break;
	}
}

void GameScene::showInfo(std::wstring info)
{
	m_infoLabel->setString(WStrToUTF8(info).c_str());
	float time = 0.5f;

	CCSequence* actions = CCSequence::createWithTwoActions(CCFadeIn::create(time), CCFadeOut::create(time));
	m_infoLabel->runAction((CCSequence*)actions->copy()->autorelease());
	m_infoLayer->setVisible(true);
	m_infoLayer->runAction(actions);
}
