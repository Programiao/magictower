#ifndef __FLOOR_H__
#define __FLOOR_H__

#include "Global.h"

class Floor : public CCObject
{
private:
	CCTMXLayer* m_layer;
	CCTMXLayer* m_animLayer;
	CCTMXTiledMap* m_map;
	CCLayer* m_gameScene;
	CCTexture2D* m_texture;
	int m_left;
	int m_bottom;
	CCSize m_size;
	bool processTileInfo(int gid, CCPoint p);
	bool animateTile(int gid, CCSprite* sprite);
	CCPoint m_pToRemove;
	
public:
	Floor(void);
	~Floor(void);

	void removeCallback();
	bool init(int left, int bottom, int index, CCTMXTiledMap* parentMap);
	CCTMXLayer* getLayer();
	void setVisible(bool v);
	CCDictionary* getInfoAt(CCPoint pos);
	void removeObjAt(CCPoint pos);
	bool playAnimation(CCPoint pos);
};

#endif //__FLOOR_H__
